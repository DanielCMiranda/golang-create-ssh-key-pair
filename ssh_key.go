package main

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"io"

	"golang.org/x/crypto/ssh"
)

// errInvalidPrivateKey will be used to wrap a RSA internal error
type errInvalidPrivateKey struct {
	inner error
}

func (e *errInvalidPrivateKey) Error() string {
	return fmt.Sprintf("invalid private key: %v", e.inner)
}

func (e *errInvalidPrivateKey) Unwrap() error {
	return e.inner
}

func (e *errInvalidPrivateKey) Is(err error) bool {
	_, ok := err.(*errInvalidPrivateKey)
	return ok
}

// SSHKeyGenerator is a factory for Public and Private key pairs
type SSHKeyGenerator interface {
	GenerateKeyPair(bitSize int) (*KeyPair, error)
}

// KeyPair centralizeds the generated public and private keys
type KeyPair struct {
	PublicKey  []byte
	PrivateKey []byte
}

type rsaKeyGenerator struct {
	// Functions encapsulated to make easier creating unit tests
	generateRSAKey  func(random io.Reader, bits int) (*rsa.PrivateKey, error)
	newSSHPublicKey func(key interface{}) (ssh.PublicKey, error)
}

// NewSSHKeyGenerator instantiates a concrete instance of SSHKeyGenerator
func NewSSHKeyGenerator() SSHKeyGenerator {
	keyGenerator := &rsaKeyGenerator{}
	keyGenerator.generateRSAKey = rsa.GenerateKey
	keyGenerator.newSSHPublicKey = ssh.NewPublicKey
	return keyGenerator
}

// GenerateKeyPair generate a RSA Public and Private key pair
func (r *rsaKeyGenerator) GenerateKeyPair(bitSize int) (*KeyPair, error) {
	privateKey, err := r.generateRSAKey(rand.Reader, bitSize)
	if err != nil {
		return nil, fmt.Errorf("error generating the private key: %w", err)
	}

	err = privateKey.Validate()
	if err != nil {
		return nil, &errInvalidPrivateKey{inner: err}
	}

	privateKeyBytes := r.encodePrivateKeyToPEM(privateKey)

	publicKeyBytes, err := r.getPublicKey(privateKey)
	if err != nil {
		return nil, fmt.Errorf("error generating the public key: %w", err)
	}

	keyPair := &KeyPair{
		PublicKey:  publicKeyBytes,
		PrivateKey: privateKeyBytes,
	}

	return keyPair, nil
}

func (r *rsaKeyGenerator) encodePrivateKeyToPEM(privateKey *rsa.PrivateKey) []byte {
	privBlock := pem.Block{
		Type:    "RSA PRIVATE KEY",
		Headers: nil,
		Bytes:   x509.MarshalPKCS1PrivateKey(privateKey),
	}

	return pem.EncodeToMemory(&privBlock)
}

func (r *rsaKeyGenerator) getPublicKey(privateKey *rsa.PrivateKey) ([]byte, error) {
	publicKey, err := r.newSSHPublicKey(&privateKey.PublicKey)
	if err != nil {
		return nil, err
	}

	return ssh.MarshalAuthorizedKey(publicKey), nil
}
