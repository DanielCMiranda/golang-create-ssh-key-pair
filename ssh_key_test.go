package main

import (
	"crypto/rand"
	"crypto/rsa"
	"errors"
	"io"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"golang.org/x/crypto/ssh"
)

func TestNewSSHKeyGenerator(t *testing.T) {
	generator := NewSSHKeyGenerator()
	assert.NotNil(t, generator)
	assert.NotNil(t, generator.(*rsaKeyGenerator).generateRSAKey)
	assert.NotNil(t, generator.(*rsaKeyGenerator).newSSHPublicKey)
}

func TestGenerateKeyPair(t *testing.T) {
	testError := errors.New("simulated error")
	testErrorRSAInternal := new(errInvalidPrivateKey)
	bitSize := 64

	validKeyPair, err := rsa.GenerateKey(rand.Reader, bitSize)
	require.NoError(t, err)

	invalidKeyPair := &rsa.PrivateKey{}

	tests := map[string]struct {
		mockedGeneratedKey   *rsa.PrivateKey
		generateKeyError     error
		newSSHPublicKeyError error
		expectedError        error
	}{
		"Generate key with success": {
			mockedGeneratedKey:   validKeyPair,
			generateKeyError:     nil,
			newSSHPublicKeyError: nil,
			expectedError:        nil,
		},
		"Error generating key": {
			mockedGeneratedKey:   nil,
			generateKeyError:     testError,
			newSSHPublicKeyError: nil,
			expectedError:        testError,
		},
		"Error invalid key pair": {
			mockedGeneratedKey:   invalidKeyPair,
			generateKeyError:     nil,
			newSSHPublicKeyError: nil,
			expectedError:        testErrorRSAInternal,
		},
		"Error creating SSH public key": {
			mockedGeneratedKey:   validKeyPair,
			generateKeyError:     nil,
			newSSHPublicKeyError: testError,
			expectedError:        testError,
		},
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			keyGenerator := NewSSHKeyGenerator()

			keyGenerator.(*rsaKeyGenerator).generateRSAKey = func(r io.Reader, b int) (*rsa.PrivateKey, error) {
				return tt.mockedGeneratedKey, tt.generateKeyError
			}

			if tt.newSSHPublicKeyError != nil {
				keyGenerator.(*rsaKeyGenerator).newSSHPublicKey = func(k interface{}) (ssh.PublicKey, error) {
					return nil, tt.newSSHPublicKeyError
				}
			}

			keyPair, err := keyGenerator.GenerateKeyPair(bitSize)

			if tt.expectedError != nil {
				ErrorIs(t, err, tt.expectedError)
				assert.Nil(t, keyPair)
				return
			}

			assert.NoError(t, err)
			assert.NotNil(t, keyPair.PublicKey)
			assert.NotNil(t, keyPair.PrivateKey)
		})
	}
}

func ErrorIs(t *testing.T, err error, expected error) bool {
	return assert.Truef(
		t,
		errors.Is(err, expected),
		"Unexpected error: %#v is not %#v",
		err,
		expected,
	)
}
