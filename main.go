package main

import (
	"io/ioutil"
	"log"
)

func main() {
	bitSize := 4096
	privateKeyFilePath := "/tmp/id_rsa"
	publicKeyFilePath := "/tmp/id_rsa.pub"

	keyGenerator := NewSSHKeyGenerator()

	keyPair, err := keyGenerator.GenerateKeyPair(bitSize)
	if err != nil {
		log.Fatal(err.Error())
	}

	err = ioutil.WriteFile(publicKeyFilePath, keyPair.PublicKey, 0600)
	if err != nil {
		log.Fatal(err.Error())
	}

	err = ioutil.WriteFile(privateKeyFilePath, keyPair.PrivateKey, 0600)
	if err != nil {
		log.Fatal(err.Error())
	}
}
